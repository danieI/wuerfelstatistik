// 1. JavaScript-Konstruktoren für HTML-Elemente

const buttonWuerfeln = document.querySelector('#buttonWuerfeln');
const sliderWuerfe = document.querySelector('#sliderWuerfe');
const wert = document.querySelector('#wert');
const td1 = document.querySelector('#td1');
const td2 = document.querySelector('#td2');
const td3 = document.querySelector('#td3');
const td4 = document.querySelector('#td4');
const td5 = document.querySelector('#td5');
const td6 = document.querySelector('#td6');

// 2. Daten
const ANZAHL_WUERFE = 600;
const anzahl = [0, 0, 0, 0, 0, 0];
wert.textContent = sliderWuerfe.wert;

// 3. Funktionsdefintionen

const wuerfeln = () => {
    // 600x würfeln
    for (let i = 0; i < ANZAHL_WUERFE; i++) {
        const wuerfel = Math.ceil(Math.random() * 6);
        anzahl[wuerfel - 1]++;
    }
}

const ausgeben = () => {
    // Anzahlen ausgeben
    td1.textContent = anzahl[0];
    td2.textContent = anzahl[1];
    td3.textContent = anzahl[2];
    td4.textContent = anzahl[3];
    td5.textContent = anzahl[4];
    td6.textContent = anzahl[5];
}

const reset = () => {
    for (let i = 0; i < 6; i++) {
        anzahl[i] = 0;
    }
}

const buttonWuerfelnClick = () => {
    reset();
    wuerfeln();
    ausgeben();
}

// 4. Eventlistener

buttonWuerfeln.addEventListener('click', buttonWuerfelnClick);
sliderWuerfe.addEventListener("input", (event) => {
    wert.textContent = event.target.wert;
});


// 5. Start
wuerfeln();
ausgeben();
